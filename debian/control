Source: libnet-ntp-perl
Section: perl
Priority: optional
Build-Depends: debhelper, perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>, gregor herrmann <gregoa@debian.org>
Standards-Version: 3.9.6
Homepage: https://metacpan.org/release/Net-NTP
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-ntp-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-ntp-perl
Testsuite: autopkgtest-pkg-perl

Package: libnet-ntp-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}, netbase
Recommends: libio-socket-inet6-perl
Multi-Arch: foreign
Description: Perl module to query NTP servers
 Net::NTP is a small perl module to query an NTP (Network Time Protocol)
 time server for the current time. It exports a single method
 (get_ntp_response) and returns an associative array based upon RFC1305 and
 RFC2030.  The response from the server is "humanized" to a point that further
 processing of the information received from the server can be manipulated.
 For example: timestamps are in epoch, so one could use the localtime function
 to produce an even more "human" representation of the timestamp.
